# -*- coding: utf-8 -*-
"""
Tests for code to get data from moonboard website .pdfs

Usage:

 in a terminal:

    1. cd folder containing package.py
    2. run `nosetests --with-coverage --cover-html --cover-package=problems_pdfs`

 from anaconda ipython console:

   * !nosetests tests

nose will find these test scripts, run them and alert you to any errors
Created on Mon Jul 27 18:40:29 2015
@author: laurence
"""
from __future__ import division, print_function
import scraping.problem_pdfs as sc

import os
import unittest
from mock import patch
from pyPdf import PdfFileReader

class TestMoonboard(unittest.TestCase):
    """
    Class for testing the moonboard code
    """
    @classmethod
    def setup_class(cls):
        """This method is run once for each class before any tests are run"""
        fwd = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        cls.data_dir = os.path.join(fwd, os.path.basename(sc.DEFAULT_DATA_PATH))
        cls.data_filename = os.path.join(cls.data_dir, r'prob7_blessed_for_test.pdf')
        cls.data_file_handle = open(cls.data_filename, 'rb')
        cls.pdf_file = PdfFileReader(cls.data_file_handle)
        cls.pdf_as_text = cls.pdf_file.getPage(0).extractText()

    @classmethod
    def teardown_class(cls):
        """This method is run once for each class _after_ all tests are run"""
        cls.data_file_handle.close()

    def set_up(self):
        """This method is run once before _each_ test method is executed"""
        pass

    def tear_down(self):
        """This method is run once after _each_ test method is executed"""
        pass


    def test_form_url_given_argument(self):
        """make sure we are using the correct url forming scheme"""
        prob_num = 99
        url_base = 'http://www.moonclimbing.com/moonboard/includes/print_pdf.php?item='
        expected = url_base + str(prob_num)
        got = sc.form_pdf_url(problem_id=prob_num)
        self.assertEqual(got, expected)

    def test_form_url_without_argument(self):
        """make sure we are using the correct url forming scheme"""
        expected = r'http://www.moonclimbing.com/moonboard/includes/print_pdf.php?item=7'
        got = sc.form_pdf_url()
        self.assertEqual(got, expected)

    def test_get_output_filename(self):
        """make sure we are using the correct url forming scheme"""
        prob_num = 42
        expected = r'.\moonboard_data\prob{0}.pdf'.format(prob_num)
        got = sc.get_output_filename(prob_num)
        self.assertEqual(got, expected)


    def test_grab_remote_pdf_makes_request(self):# pylint: disable=invalid-name
        """
        Test open a the blessed pdf in the data directory
        make sure we get a the same text as we loaded in`setup_class`
        """
        pdf_file = self.data_filename
        file_url = r'file:///' + pdf_file
        if os.name == 'nt':
            file_url = file_url.replace('\\', '/')
        got = sc.grab_remote_pdf(file_url)
        self.assertEqual(got.getPage(0).extractText(), self.pdf_as_text)


    @patch('scraping.problem_pdfs.build_opener')
    def test_grab_remote_pdf_ret_none_on_if_pdf_error(self, mock_opener):# pylint: disable=invalid-name
        """
        test we get `None` back if we get a response with FPDF Error in it
        """
        mock_opener.return_value = MockOpener()
        got = sc.grab_remote_pdf(r'www.example.com')
        self.assertTrue(got is None)

    def test_pdffile_to_text(self):
        """
        can we get text ok from file. c.f blessed data file
        """
        got = sc.pdffile_to_text(self.pdf_file)
        self.assertEqual(got, self.pdf_as_text)

# pylint: disable=too-few-public-methods
# happy for fake classes to have few public methods
class MockOpener(object):
    """
    fake some behaviour of `six.moves.urllib.request..build_opener`
    for testing

    needs utility class MockResponse
    """
    def __init__(self):
        pass
    @staticmethod
    def open(_):
        """
        more fakary for getting the tests to work
        """
        return MockResponse

class MockResponse(object):
    """
    fake the response.read() funtionality as in
    `build_opener().open(request).read()`
    for testing
    """
    def __init__(self):
        pass
    @staticmethod
    def read():
        """
        pretend we got a FPDF error message because we asked the moon site
        for a non-existant problem
        """
        return sc.ERR_MESS_PART
# pylint: enable=too-few-public-methods

if __name__ == '__main__':
    unittest.main()
