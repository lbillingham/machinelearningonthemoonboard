# -*- coding: utf-8 -*-
"""
Fetch a .pdf describing a moonboard problem and save it to disk.
Also contains a hint of how to parse the pdf to get the problem
details
Created on Thu Mar 05 19:14:24 2015

@author: laurence
"""
from six.moves.urllib.request import Request, build_opener
from pyPdf import PdfFileWriter, PdfFileReader
from StringIO import StringIO
import os.path as pth
import pandas as pd
import re

# TODO:
#  * testing
#  * make a ProblemPdf() object which has `id`, `url`, `outfile` as
#        attributes and most of these functions _function except download_pdf


BASE_URL = 'http://www.moonclimbing.com/moonboard/includes/print_pdf.php?item={0}'
DEFAULT_DATA_PATH = pth.join('.', 'moonboard_data')
ERR_MESS_PART = 'FPDF error'

def download_pdf(problem_id, output_file=None):
    """
    Downloads a .pdf describing a Moonboard problem from the moon website.

    Parameters
    ----------
    problem_id : int, optional, `default=7`
        The number that identifies a specific problem on the moonboard.
        The validity scheme is somewhat strange; if we fail to find a problem
        on the Moon website we simply `return None`.
        The default value will download 'Problem 2 by Ben Moon'(`problem_id=7`)
        the lowest id available.

    output_file : string, optional, `default=None`
        Path to the ouput file. If `None` specified then we default to
        prob<`problem_id`>.pdf in the hard coded DEFAULT_DATA_PATH.

    Returns
    -------
    None:

    Side effects:
    -------------
        Saves the .pdf file, see `Parameters-output_file` for location.

    """
    url = form_pdf_url(problem_id)
    if None is output_file:
        output_file = get_output_filename(problem_id)

    pdf_file = grab_remote_pdf(url)
    if pdf_file is not None:
        write_pdf_to_disk(pdf_file, output_file)


def form_pdf_url(problem_id=7):
    """
    Get the URL for the .pdf describing a particular Moonboard problem.

    Parameters
    ----------
    problem_id : int, optional, `default=7`
        The number that identifies a specific problem on the moonboard.
        The validity scheme is somewhat strange; if we fail to find a problem
        on the Moon website we simply `return None`.
        The default value will download 'Problem 2 by Ben Moon'(`problem_id=7`)
        the lowest id available.

    Returns
    -------
    url : string,
        The URL where the .pdf describing problem `problem_id` may be found.
    """
    url = BASE_URL.format(problem_id)
    return url

def get_output_filename(problem_id):
    """
    Returns the path to an output file for saving a Moonboard problem description
    .pdf.

    Parameters
    ----------
    problem_id : int,
        The number that identifies a specific problem on the moonboard.

    Returns
    -------
    output_file : string,
        The path in which to save the .pdf file.
    """
    output_file = pth.join(DEFAULT_DATA_PATH,
                           'prob{0}'.format(problem_id) + '.pdf')
    return output_file

def grab_remote_pdf(url):
    """
    Return a `pyPdf.PdfFileReader` object containing the resource at `url`.
    Returns `None` if reqest returns a FDPF error.

    Parameters
    ----------
    url :  string
     The URL to which the request is made

    Returns
    -------
    pdf_file : pyPdf.PdfFileReader object,
        The resource at `url` as a pyPdf.PdfFileReader.
    """
    req = Request(url)
    opener = build_opener()
    response = opener.open(req)
    file_as_str = response.read()
    if ERR_MESS_PART in file_as_str:
        return None
    memory_file = StringIO(file_as_str)
    response.close()
    pdf_file = PdfFileReader(memory_file)
    return pdf_file

def write_pdf_to_disk(pdf_file, filename_out):
    """
    Write a `pyPdf.PdfFileReader` object to disk.

    Parameters
    ----------
    pdf_file : pyPdf.PdfFileReader object,
        The contents of the file to be written.

    filename_out : string,
        The full path of the file we want to write.

    Returns
    -------
    None

    Side effects
    ----------
    pdf_file : pyPdf.PdfFileReader object,
        Writes the contents of `pdf_file` to disk at `filename_out`
    """
    writer = PdfFileWriter()
    for page_num in range(pdf_file.getNumPages()):
        current_page = pdf_file.getPage(page_num)
        writer.addPage(current_page)

    with open(filename_out, 'wb') as output_stream:
        writer.write(output_stream)

def load_pdf_from_disk(file_path):
    """
    Load a problem .pdf into memory and grab the problem data

    Parameters
    ----------
    file_path : string,
        The path from which to load the .pdf file.

    Returns
    -------
    text:
        the text found within the file
    """
    with open(file_path, 'rb') as input_file:
        try:
            pdf_file = PdfFileReader(input_file)
        except IOError as err:
            if 2 != err.errno:
                raise IOError('Not a valid .pdf file {0}'.format(file_path))
        pages_text = pdffile_to_text(pdf_file)
    return pages_text


def pdffile_to_text(pdf_file):
    """
    Extract the text from a problem .pdf file in memory.
    N.B. assumes that the .pdf file is one page

    Parameters
    ----------
    pdf_file : pyPdf.PdfFileReader object,
        The contents of the file from which to grab the text.

    Returns
    -------
    text:
        the text found within the file
    """
    pages_text = []
    for page_num in xrange(pdf_file.getNumPages()):
        current_page = pdf_file.getPage(page_num)
        text = current_page.extractText()
        pages_text.append(text)
    if len(pages_text) == 1: #if we only have a single page (as we expect)
                                             #  don't return a list, just that page's data
        pages_text = pages_text[0]
    return pages_text


def parser(text):
    """
    Extract the problem data from the .pdf file's text and return a
    `pandas.Series`.

    Parameters
    ----------
    text:
        the text found within a problem .pdf file

    Returns
    -------
    `pandas.Series`:
        describes the problem in pdf text
    """
    prob = {}
    find_hold_set = r'(?P<hold_set>[\w\s]+#[\d-]{3,7})'

    find_setup = r'[Ss]etup\s+(?P<setup>\d{1,2})'
    match = re.search(find_hold_set + find_setup, text)
    if match:
        prob['hold_set'] = match.groupdict()['hold_set']
        prob['setup'] = match.groupdict()['setup']

    find_name = r'[Ss]etup\s*\d{1,2}(.*?)\s*[Ss]et\s+[bB]y'
    name = re.findall(find_name, text)
    if name:
        prob['name'] = name[0]

    find_grade = r'[gG]rade\s+(?P<grade>[0-9]+[A-Ca-c])'
    find_setter = r'[Ss]et\s+[bB]y\s+(?P<setter>[A-Za-z\s]+)'
    match = re.search(find_setter + find_grade, text)
    if match:
        prob['grade'] = match.groupdict()['grade']
        prob['setter'] = match.groupdict()['setter']

    find_finish = r'(?P<hold>[A-Ka-k][0-9]{1,2})\s*\(Finish\)'
    finish_holds = re.findall(find_finish, text)
    if len(finish_holds) == 1:
        finish_holds = 2 * finish_holds # double count last hold if finish matched

    find_start = r'(?P<hold>[A-Ka-k][0-9]{1,2})\s*\(Start\)'
    start_holds = re.findall(find_start, text)
    if len(start_holds) == 1:
        start_holds = 2 * start_holds # double count first hold if start matched

    find_holds = r'\(Start\)(?P<data>.+?)\(Finish\)'
    m_holds = re.search(find_holds, text)
    if m_holds:
        holds_text = m_holds.groupdict()['data'].strip()
    find_a_hold = r'[A-Ka-k]{1}[0-9]{1,2}'
    holds = re.findall(find_a_hold, holds_text)
    holds = holds[:-1] # drop final match as have already accounted for it
                                            # in finish_holds
    prob_series = pd.Series(prob)

    idx = (['start_1', 'start_2']+ range(3, len(holds) + 3) +
           ['end_1', 'end_2'])

    hold_series = pd.Series(index=idx,
                            data=start_holds + holds + finish_holds)
    output = prob_series.append(hold_series)
    return output


def get_problem_series(prob_id=7, download_new_files=False):
    """
    Return a series describing a given problem.
    Optionally save a .pdf file describing the problem from the
    moon website.

    Parameters
    ----------
    problem_id : int, optional, `default=7`
        The number that identifies a specific problem on the moonboard.
        The validity scheme is somewhat strange; if we fail to find a problem
        on the Moon website we simply `return None`.
        The default value will download 'Problem 2 by Ben Moon'(`problem_id=7`)

    download_new_files : bool, optional, `default=False`
        Will we save a .pdf file describing the problem to the hard disk?

    Returns
    -------
    `pandas.Series`:
        describes the problem given by `prob_id`
    """
    probfile = get_output_filename(prob_id)
    if pth.isfile(probfile):
        text = load_pdf_from_disk(probfile)
    else:
        url = form_pdf_url(prob_id)
        pdf_file = grab_remote_pdf(url)
        if None is pdf_file:
            return None
        text = pdffile_to_text(pdf_file)
        if download_new_files:
            write_pdf_to_disk(pdf_file, probfile)

    prob = parser(text)
    return prob

def get_problems_df(problem_ids, download_new_files=False):
    """
    Return a dataframe describing a sequence of problems.
    Optionally save the .pdf files describing the problems from the
    moon website.

    Parameters
    ----------
    problem_ids : list of ints,
        The numbers that identifies a specific problems on the moonboard.
        The validity scheme is somewhat strange; if we fail to find a problem
        on the Moon website we simply `return None`.

    download_new_files : bool, optional, `default=False`
        Will we save a .pdf files describing the problems to the hard disk?

    Returns
    -------
    `pandas.DataFrame`:
        describes the problem given by `prob_id`
    """
    probs_df = pd.DataFrame()
    for pid in problem_ids:
        prob_series = get_problem_series(pid, download_new_files)
        if None is prob_series:
            continue
        probs_df[pid] = prob_series
    return probs_df

def save_excel_file(problems_dataframe, filename='problems.xlsx'):
    """
    Save a DataFrame describing a number of problems to hard disk.

    Parameters
    ----------
    problems_dataframe :
        `pandas.DataFrame` describing a number of moonboard problems

    filename : string, optional, `default=problems.xlsx`
        Name of the excel file to write.

    Returns
    -------
    None

    Side effects
    ------------
        Writes the contents of `problems_dataframe` to disk to a file named `filename`
        Folder set by module level constant DEFAULT_DATA_PATH
    """
    probs_df = problems_dataframe.transpose()
    out_file = pth.join(pth.dirname(get_output_filename(0)), filename)
    probs_df.to_excel(out_file)





if '__main__' == __name__:
    pass
#    probs_df = get_problems_df([22, 7, 14, 526])
#    for problem_id in [7, 22, 24, 500]:
#        download_pdf(problem_id=problem_id)
