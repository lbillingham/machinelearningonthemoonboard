# Create new problems for the moonboard using machine learning#

## What is a moonboard?
The [moonboard](http://www.moonclimbing.com/moonboard/page.php?page=I:1) is a training tool for climbers invented by climbing legend [Ben Moon](http://www.moonclimbing.com/blog/climbers/ben-moon/). The board is a rectangle of plywood, about ~3.5m * 2.5m, which overhangs by 40º. It hosts 198 recessed T-nuts in a regular grid (0.2m * 0.2m spacing) to which resin [climbing holds](http://www.moonclimbing.com/gear/climbing-holds.html) can be fixed.

Completing a problem on the board consists of moving, in sequence between, a pre-determined set of holds. How difficult it is to complete each problem difficulty is graded on the [Fontainebleau grade scale](http://en.wikipedia.org/wiki/Grade_%28bouldering%29#Fontainebleau_grades).

## What are we trying to do?

This is a project to gather information on [existing moonboard problems](http://www.moonclimbing.com/moonboard/page.php?page=V:1&problem=7&filterHolds=0&filterGradeFrom=0&filterGradeTo=0&filterSetby=0&search_text=&starting=1131): 

* how fun are they according to user ratings?
* how difficult are they?
* which holds do they use?
* how big are the moves between holds?
* ...

Then use machine learning techniques to

* recommend another problem that is similar to one the user enjoyed
    * perhaps at a progressive grade
* 'discover' new problems at a given grade

This project is a bit of fun and in no way endorsed by Moon. Any views or recommendations represented here are the sole opinion of the authors.


Climbing and mountaineering are activities with a danger of personal injury or death. Participants in these activities should be aware of and accept these risks and be responsible for their own actions and involvement.

### Who do I talk to? ###

* Contact me through https://bitbucket.org/lbillingham/